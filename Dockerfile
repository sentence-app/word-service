FROM node:16-alpine

WORKDIR /usr/app

ARG DB
ENV WORD_SERVICE_ENV null
ENV TERM xterm

RUN \
    echo "==> Install app..."                && \
    npm install --omit=dev json-server  && \
    \
    \
    echo "==> Remove unused temp..."         && \
    rm -rf /root/.npm                  \
           /usr/lib/node_modules/npm

COPY /db/$DB /usr/app/db.json
COPY /server.js /usr/app

EXPOSE 8080

CMD ["node", "server.js"]
