// server.js
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()

server.use(middlewares)
// Env variable injection into header
server.use((req, res, next) => {

    res.header('X-Word-Service-Env', process.env.WORD_SERVICE_ENV)
    next()
})
server.use(router)
server.listen(8080, () => {
    console.log('JSON Server is running')
})